import datetime
import importlib
import re
from typing import Optional, List

from telegram import Message, Chat, Update, Bot, User
from telegram import ParseMode, InlineKeyboardMarkup, InlineKeyboardButton, ReplyKeyboardMarkup, KeyboardButton
from telegram.error import Unauthorized, BadRequest, TimedOut, NetworkError, ChatMigrated, TelegramError
from telegram.ext import CommandHandler, Filters, MessageHandler, CallbackQueryHandler
from telegram.ext.dispatcher import run_async, DispatcherHandlerStop, Dispatcher
from telegram.utils.helpers import escape_markdown

from tg_bot import dispatcher, updater, TOKEN, WEBHOOK, SUDO_USERS, OWNER_ID, DONATION_LINK, CERT_PATH, PORT, URL, LOGGER, \
    ALLOW_EXCL
# needed to dynamically load modules
# NOTE: Module order is not guaranteed, specify that in the config file!
from tg_bot.modules import ALL_MODULES
from tg_bot.modules.helper_funcs.chat_status import is_user_admin
from tg_bot.modules.helper_funcs.misc import paginate_modules
from tg_bot.modules.translations.strings import tld, tld_help 
from tg_bot.modules.connection import connected

PM_START = """Konnichiwa  {}, \n\n My name is {} - \n I can manage both you and your groups! \n
Hit /help to see what I can do! :3 
\n I'm built in python3. \n Maintained By [this awesome person](https://t.me/KingOfElephants) ! :3\nI'm fully open sourced. You can find out what makes me tick [here](https://gitlab.com/gajrajtherock/julieBot).
\n If you're enjoying using me and would like to help me s